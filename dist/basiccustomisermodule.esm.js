import 'vue';
import 'bootstrap/dist/css/bootstrap.css';

var script = {
    name: 'Basiccustomisermodule', // vue component name
    props: {
      productID: {
        type: Number,
        default: 174
      },
      sessionID: {
        type: String,
        default: "1111"
      },
    },
    data: function data() {
        return {
          product: {},
          productImage: "",
          artwork: {
            left: "288",
            top: "528",
            width: "342",
            height: "113",
            cWidth: 45 * 10,
            cHeight: 15 * 10
          },
          text: {
            value: ""
          }
        };
    },
    mounted: function mounted () {
      var this$1 = this;

      window.fetch(("https://biccustomiserportal.engagisdemo.com.au/wp-json/wc/v2/products/" + (this.productID) + "?consumer_key=ck_f216384f5e9db05dba249d0b487067d6190e3784&consumer_secret=cs_c71b5becdea75797d1beb5b0ce425cd07f9b9ef9")).then(function (response) {
        return response.json();
      }).then(function (json) {
        if(json.data){
          if(json.data.status === 404) ;
        } else {
          this$1.product = json;
          this$1.product.images.forEach(function (image) {
            if(image.position === 0) {
              this$1.productImage = image.src;
            }
          });
        }
      });
    },
    methods: {
      process: function process(e){
        this.text.value = e.target.value.replace(/(?:\r\n|\r|\n)/g, '<br/>');
        this.text.raw = e.target.value;
      },
      changeArtwork: function changeArtwork(e){
        var this$1 = this;

        var ctx = document.getElementsByClassName('basiccustomisermodule_customiser_image_artwork_canvas')[0].getContext('2d');
        

        console.log( e.target.files[0] );
        switch(e.target.files[0].type){
          case "image/svg+xml":
            var svgImage = new Image;
            svgImage.onload = function () {
              ctx.drawImage(svgImage, 0,0, this$1.artwork.cWidth, this$1.artwork.cHeight);
            };
            svgImage.src = URL.createObjectURL(e.target.files[0]);
            break;
          case "image/png":
          case "image/jpeg":
            var img = new Image;
            img.onload = function () {
              ctx.drawImage(img, 0,0, this$1.artwork.cWidth, this$1.artwork.cHeight);
            };
            img.src = URL.createObjectURL(e.target.files[0]);
            break;
          default:
        }
      }
    }
};

function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier
/* server only */
, shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
  if (typeof shadowMode !== 'boolean') {
    createInjectorSSR = createInjector;
    createInjector = shadowMode;
    shadowMode = false;
  } // Vue.extend constructor export interop.


  var options = typeof script === 'function' ? script.options : script; // render functions

  if (template && template.render) {
    options.render = template.render;
    options.staticRenderFns = template.staticRenderFns;
    options._compiled = true; // functional template

    if (isFunctionalTemplate) {
      options.functional = true;
    }
  } // scopedId


  if (scopeId) {
    options._scopeId = scopeId;
  }

  var hook;

  if (moduleIdentifier) {
    // server build
    hook = function hook(context) {
      // 2.3 injection
      context = context || // cached call
      this.$vnode && this.$vnode.ssrContext || // stateful
      this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext; // functional
      // 2.2 with runInNewContext: true

      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__;
      } // inject component styles


      if (style) {
        style.call(this, createInjectorSSR(context));
      } // register component module identifier for async chunk inference


      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier);
      }
    }; // used by ssr in case component is cached and beforeCreate
    // never gets called


    options._ssrRegister = hook;
  } else if (style) {
    hook = shadowMode ? function () {
      style.call(this, createInjectorShadow(this.$root.$options.shadowRoot));
    } : function (context) {
      style.call(this, createInjector(context));
    };
  }

  if (hook) {
    if (options.functional) {
      // register for functional component in vue file
      var originalRender = options.render;

      options.render = function renderWithStyleInjection(h, context) {
        hook.call(context);
        return originalRender(h, context);
      };
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate;
      options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
    }
  }

  return script;
}

var normalizeComponent_1 = normalizeComponent;

var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
function createInjector(context) {
  return function (id, style) {
    return addStyle(id, style);
  };
}
var HEAD = document.head || document.getElementsByTagName('head')[0];
var styles = {};

function addStyle(id, css) {
  var group = isOldIE ? css.media || 'default' : id;
  var style = styles[group] || (styles[group] = {
    ids: new Set(),
    styles: []
  });

  if (!style.ids.has(id)) {
    style.ids.add(id);
    var code = css.source;

    if (css.map) {
      // https://developer.chrome.com/devtools/docs/javascript-debugging
      // this makes source maps inside style tags work properly in Chrome
      code += '\n/*# sourceURL=' + css.map.sources[0] + ' */'; // http://stackoverflow.com/a/26603875

      code += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) + ' */';
    }

    if (!style.element) {
      style.element = document.createElement('style');
      style.element.type = 'text/css';
      if (css.media) { style.element.setAttribute('media', css.media); }
      HEAD.appendChild(style.element);
    }

    if ('styleSheet' in style.element) {
      style.styles.push(code);
      style.element.styleSheet.cssText = style.styles.filter(Boolean).join('\n');
    } else {
      var index = style.ids.size - 1;
      var textNode = document.createTextNode(code);
      var nodes = style.element.childNodes;
      if (nodes[index]) { style.element.removeChild(nodes[index]); }
      if (nodes.length) { style.element.insertBefore(textNode, nodes[index]); }else { style.element.appendChild(textNode); }
    }
  }
}

var browser = createInjector;

/* script */
var __vue_script__ = script;

/* template */
var __vue_render__ = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"basiccustomisermodule container-fluid"},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"basiccustomisermodule_controls col-3"},[_c('h1',[_vm._v("Customiser Controls")]),_vm._v(" "),_c('h3',[_vm._v("Text")]),_vm._v(" "),_c('textarea',{directives:[{name:"model",rawName:"v-model",value:(_vm.text.raw),expression:"text.raw"}],attrs:{"type":"text"},domProps:{"value":(_vm.text.raw)},on:{"keyup":_vm.process,"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.text, "raw", $event.target.value);}}}),_vm._v(" "),_c('h3',[_vm._v("Image")]),_vm._v(" "),_c('input',{attrs:{"type":"file"},on:{"change":_vm.changeArtwork}})]),_vm._v(" "),_c('div',{staticClass:"basiccustomisermodule_customiser col-6"},[_c('h1',[_vm._v("Customiser")]),_vm._v(" "),_c('div',{staticClass:"basiccustomisermodule_customiser_canvas"},[_c('img',{staticClass:"basiccustomisermodule_customiser_canvas_image",attrs:{"src":_vm.productImage}}),_vm._v(" "),_c('div',{staticClass:"basiccustomisermodule_customiser_image_artwork",style:({ left: _vm.artwork.left + "px", top: _vm.artwork.top + "px", width: _vm.artwork.width + "px", height: _vm.artwork.height + "px" })},[_c('canvas',{staticClass:"basiccustomisermodule_customiser_image_artwork_canvas",style:({width: _vm.artwork.width + "px"}),attrs:{"width":_vm.artwork.cWidth,"height":_vm.artwork.cHeight}})]),_vm._v(" "),_c('div',{staticClass:"basiccustomisermodule_customiser_image_text",style:({ left: _vm.artwork.left + "px", top: _vm.artwork.top + "px", width: _vm.artwork.width + "px", height: _vm.artwork.height + "px" }),domProps:{"innerHTML":_vm._s(_vm.text.value)}})])]),_vm._v(" "),_vm._m(0)])])};
var __vue_staticRenderFns__ = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"basiccustomisermodule_pricing col-3"},[_c('h1',[_vm._v("Price")])])}];

  /* style */
  var __vue_inject_styles__ = function (inject) {
    if (!inject) { return }
    inject("data-v-dcaa1072_0", { source: ".basiccustomisermodule[data-v-dcaa1072]{width:1800px}.basiccustomisermodule_customiser_canvas[data-v-dcaa1072]{position:relative;transform:scale(.7,.7);transform-origin:top left}.basiccustomisermodule_customiser_canvas img[data-v-dcaa1072]{display:block}.basiccustomisermodule_customiser_image_artwork[data-v-dcaa1072]{z-index:100;position:absolute}.basiccustomisermodule_customiser_image_text[data-v-dcaa1072]{overflow:hidden;z-index:99;position:absolute;background-color:red}", map: undefined, media: undefined });

  };
  /* scoped */
  var __vue_scope_id__ = "data-v-dcaa1072";
  /* module identifier */
  var __vue_module_identifier__ = undefined;
  /* functional template */
  var __vue_is_functional_template__ = false;
  /* style inject SSR */
  

  
  var component = normalizeComponent_1(
    { render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ },
    __vue_inject_styles__,
    __vue_script__,
    __vue_scope_id__,
    __vue_is_functional_template__,
    __vue_module_identifier__,
    browser,
    undefined
  );

// Import vue component

// install function executed by Vue.use()
function install(Vue) {
  if (install.installed) { return; }
  install.installed = true;
  Vue.component('Basiccustomisermodule', component);

}

// Create module definition for Vue.use()
var plugin = {
  install: install,
};

// To auto-install when vue is found
/* global window global */
var GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}

// Inject install function into component - allows component
// to be registered via Vue.use() as well as Vue.component()
component.install = install;

// It's possible to expose named exports when writing components that can
// also be used as directives, etc. - eg. import { RollupDemoDirective } from 'rollup-demo';
// export const RollupDemoDirective = component;

export default component;
